var config   = require('./gulpconfig.json'),
    tasks    = { js:[], less:[] };

var inquirer = require('inquirer'),
    argv     = require('yargs').default('compress', 'true').argv,
    exec     = require('child_process').exec,
    compress = (argv.compress != 'false');

var gulp     = require('gulp'),
    gulpif   = require('gulp-if'),
    plumber  = require('gulp-plumber'),
    gutil    = require('gulp-util'),
    notify   = require("gulp-notify"),
    rename   = require('gulp-rename'),
    less     = require('gulp-less'),
    include  = require('gulp-include'),
    uglify   = require('gulp-uglify');

var LessPluginCleanCSS    = require('less-plugin-clean-css'),
    LessPluginAutoPrefix  = require('less-plugin-autoprefix'),
    CombineMediaQueries   = require('less-plugin-group-css-media-queries'),
    CleanCSS              = new LessPluginCleanCSS({ advanced: false }),
    Autoprefix            = new LessPluginAutoPrefix({ browsers: ['> 1%', 'last 2 versions', 'IE >= 8', 'Firefox ESR', 'Opera 12.1'] }),
    LessPlugins           = (compress)? [CombineMediaQueries, CleanCSS, Autoprefix] : [Autoprefix];

if (!compress)
    gutil.log(gutil.colors.red('Compression is turned OFF.'));

for (var group in config.build.less)
    tasks.less.push('build:less:' + group);

for (var group in config.build.js)
    tasks.js.push('build:js:' + group);

tasks.less.forEach(function(task){
    gulp.task(task, function(done){
        var group = task.split(':').pop();
        for (var src in config.build.less[group]) {
            var pipeline = gulp.src(src)
                .pipe(plumber({
                    errorHandler: notify.onError({
                        title: 'Error running ' + task,
                        message: "<%= error.message %>"
                    })}
                ))
                .pipe(less({ plugins: LessPlugins }))
                .pipe(rename(src.split('/').pop().replace('.less', '.min.css')))
            ;
            config.build.less[group][src].forEach(function(path){
                pipeline.pipe(gulp.dest(path))
            });
        }
        done();
    });
});
gulp.task('build:less', gulp.parallel(tasks.less));

tasks.js.forEach(function(task){
    gulp.task(task, function(done){
        var group = task.split(':').pop();
        for (var src in config.build.js[group]) {
            var pipeline = gulp.src(src)
                .pipe(plumber({
                    errorHandler: notify.onError({
                        title: 'Error running ' + task,
                        message: "<%= error.message %> in line no. <%= error.lineNumber %>"
                    })}
                ))
                .pipe(include())
                .pipe(gulpif(compress, uglify()))
                .pipe(rename(src.split('/').pop().replace('.js', '.min.js')))
            ;
            config.build.js[group][src].forEach(function(path){
                pipeline.pipe(gulp.dest(path))
            });
        }
        done();
    });
});
gulp.task('build:js', gulp.parallel(tasks.js));

function WordPress(done){
    inquirer.prompt([
        {
            type: 'input',
            name: 'DB_NAME',
            message: 'DB_NAME:',
            validate: function(input){ return (!input)? 'DB_NAME is required.' : true ; }
        },
        {
            type: 'input',
            name: 'DB_USER',
            message: 'DB_USER:',
            default: 'root'
        },
        {
            type: 'input',
            name: 'DB_PASS',
            message: 'DB_PASS:',
            default: 'root'
        },
        {
            type: 'input',
            name: 'DB_HOST',
            message: 'DB_HOST:',
            default: 'localhost'
        }
    ], function(obj){
        // Prepare commands
        var commands = [
            'cd /srv/www/' + __dirname.split('/').pop(),
            'wp core download',
            'wp core config --dbname=' + obj.DB_NAME + ' --dbuser=' + obj.DB_USER + ' --dbpass=' + obj.DB_PASS + ' --dbhost=' + obj.DB_HOST + ' --extra-php <<PHP\ndefine( \'WP_DEBUG\', true );\ndefine( \'WP_LOCAL_DEV\', true );\nPHP'
        ];
        // Execute commands
        gutil.log(gutil.colors.cyan('Downloading and configuring WordPress. Please wait...'));
        exec('vagrant ssh -- -t "' + commands.join(';') + '"', function(err, stdout, stderr){
            if (err) {
                gutil.log(gutil.colors.red(err));
            } else {
                gutil.log(gutil.colors.green('Successfully downloaded and configured WordPress!'));
            }
            done();
        });
    });
}

function watch(){
    config.watch.forEach(function(obj){
        for (var group in obj.build) {
            gutil.log(gutil.colors.cyan('Watching ' + obj.files));
            gulp.watch(obj.files, gulp.series(
                'build:' + group + ':' + obj.build[group]
            ));
        }
    });
}

gulp.task('watch', watch);
gulp.task('build', gulp.parallel(['build:less', 'build:js']));
gulp.task('build:all', gulp.parallel(['build:less', 'build:js']));
gulp.task('wp:init', WordPress);
