$(document).ready(function(){
    //Handles menu drop down

	$('.offcanvas-toggle').on('click', function() {
		  $('body').toggleClass('offcanvas-expanded');
		});


	$('.nav--mobile li a').on('click', function() {
	  $('body').removeClass('offcanvas-expanded');
	});

	$(document).ready(function () {
    	$(".dropdown-toggle").click();
	});

	$('.map').click(function () {
    	$('.map iframe').css("pointer-events", "auto");
	});	

	$('.fancybox').fancybox({
		'speedIn'		:	600, 
		'speedOut'		:	200, 
		'overlayShow'	:	false,
		'padding'		: 	0
	});
});